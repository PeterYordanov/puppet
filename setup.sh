#!/bin/bash

echo "192.168.99.100 salt-server.dof.lab salt-server" >> /etc/hosts
echo "192.168.99.101 salt-client-1.dof.lab salt-client-web" >> /etc/hosts
echo "192.168.99.102 salt-client-2.dof.lab salt-client-db" >> /etc/hosts

dnf install -y https://yum.puppetlabs.com/puppet-release-el-8.noarch.rpm